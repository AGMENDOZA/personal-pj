package pe.gob.pj.personal.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_PERSONAL", schema = "ADMINPJ")
public class VPersonal {
	@Id
	private String idppicodigo;
	private String mppiappater;
	private String mppiapmater;
	private String mppinombres;
	private String perestado;
	private String idclpcodigo;
	@Column(name = "C_CONDLABORAL")
	private String cCondlaboral;

	public VPersonal() {

	}

	public String getIdppicodigo() {
		return idppicodigo;
	}

	public void setIdppicodigo(String idppicodigo) {
		this.idppicodigo = idppicodigo;
	}

	public String getMppiappater() {
		return mppiappater;
	}

	public void setMppiappater(String mppiappater) {
		this.mppiappater = mppiappater;
	}

	public String getMppiapmater() {
		return mppiapmater;
	}

	public void setMppiapmater(String mppiapmater) {
		this.mppiapmater = mppiapmater;
	}

	public String getMppinombres() {
		return mppinombres;
	}

	public void setMppinombres(String mppinombres) {
		this.mppinombres = mppinombres;
	}

	public String getPerestado() {
		return perestado;
	}

	public void setPerestado(String perestado) {
		this.perestado = perestado;
	}

	public String getIdclpcodigo() {
		return idclpcodigo;
	}

	public void setIdclpcodigo(String idclpcodigo) {
		this.idclpcodigo = idclpcodigo;
	}

	public String getcCondlaboral() {
		return cCondlaboral;
	}

	public void setcCondlaboral(String cCondlaboral) {
		this.cCondlaboral = cCondlaboral;
	}

}
