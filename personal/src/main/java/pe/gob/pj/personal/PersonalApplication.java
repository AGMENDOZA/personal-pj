package pe.gob.pj.personal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalApplication.class, args);
		
		System.out.println("====================================");
		System.out.println("=================Hola Mundo===================");
		System.out.println("====================================");
	}

}
